import { PrismaClient } from "@prisma/client"
import fs from "fs"
import path from "path"

const prisma = new PrismaClient()

async function main() {
  const crashDataDir = `${__dirname}/../crashes`
  const files = fs.readdirSync(crashDataDir)

  for await (const file of files) {
    const filePath = path.join(crashDataDir, file)
    console.log(`Loading: ${filePath}...`)
    await parseData(filePath)
  }
}

async function parseData(dataPath: string) {
  const data = fs.readFileSync(dataPath)
  const crashData = JSON.parse(data.toString())

  for await (const crash of crashData.features) {
    const {
      RKY,
      DOA,
      TOA,
      LOCAT,
      TYPE,
      ACTION,
      NUV,
      NOI,
      PED,
      BIKE,
      MC,
      MOPED,
      TRAIN,
      TRUCK,
      BUS,
    } = crash.properties

    const time = parseTime(TOA)
    const dateTime = new Date(DOA)
    if (!time.unknown) {
      dateTime.setHours(Number(time.hour))
      dateTime.setMinutes(Number(time.min))
    }

    // console.log(`Working on record ${RKY}`)
    // console.log(`DateTime: ${dateTime.toISOString()}`)
    try {
      // console.log(`Working on record ${RKY}`)

      await prisma.crashes.upsert({
        create: {
          caseNumber: RKY,
          dateOfCollision: dateTime,
          location: LOCAT,
          type: TYPE,
          action: ACTION,
          numberOfVehicles: NUV,
          numberOfInjured: NOI,
          pedestrian: coerceToBool(PED),
          bike: coerceToBool(BIKE),
          motorcycle: coerceToBool(MC),
          moped: coerceToBool(MOPED),
          train: coerceToBool(TRAIN),
          truck: coerceToBool(TRUCK),
          bus: coerceToBool(BUS),
        },
        update: {
          dateOfCollision: dateTime,
          location: LOCAT,
          type: TYPE,
          action: ACTION,
          numberOfVehicles: NUV,
          numberOfInjured: NOI,
          pedestrian: coerceToBool(PED),
          bike: coerceToBool(BIKE),
          motorcycle: coerceToBool(MC),
          moped: coerceToBool(MOPED),
          train: coerceToBool(TRAIN),
          truck: coerceToBool(TRUCK),
          bus: coerceToBool(BUS),
        },
        where: { caseNumber: RKY },
      })
    } catch (error) {
      console.error(error)
      // process.exit(1)
    }
  }

  // crashData.features.forEach(async (feature: any) => {
  //   const {
  //     RKY,
  //     DOA,
  //     TOA,
  //     LOCAT,
  //     TYPE,
  //     ACTION,
  //     NUV,
  //     NOI,
  //     PED,
  //     BIKE,
  //     MC,
  //     MOPED,
  //     TRAIN,
  //     TRUCK,
  //     BUS,
  //   } = feature.properties

  //   const time = parseTime(TOA)
  //   const dateTime = new Date(DOA)
  //   if (!time.unknown) {
  //     dateTime.setHours(Number(time.hour))
  //     dateTime.setMinutes(Number(time.min))
  //   }

  // })
}

function coerceToBool(value: string) {
  if (value === "YES") {
    return true
  }
  return false
}

function parseTime(
  time: number
): { hour: string; min: string; unknown: boolean } {
  const timeString = String(time)

  if (timeString === "9999") {
    return { hour: "00", min: "00", unknown: true }
  }

  if (timeString.length === 4) {
    const hour = timeString.substr(0, 2)
    const min = timeString.substr(2, 2)
    return { hour, min, unknown: false }
  }

  if (timeString.length === 3) {
    const hour = timeString.substr(0, 1)
    const min = timeString.substr(1, 2)
    return { hour, min, unknown: false }
  }

  return { hour: "", min: "", unknown: true }
}

main()
  .catch((e) => {
    console.error(e)
    throw e
  })
  .finally(async () => {
    await prisma.$disconnect()
    process.exit()
  })
